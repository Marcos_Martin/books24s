Rails.application.routes.draw do
  root "books#index"

  post 'basket/set_book' => 'basket#set_book', as: 'set_book'
  delete 'reset_basket' => 'basket#reset_basket'
  delete 'delete_basket_book/:id(.:format)', as: "delete_basket_book",
         to: 'basket#delete_basket_book'
  get 'basket/change_flat' => 'basket#change_flat', as: 'change_flat'

end
