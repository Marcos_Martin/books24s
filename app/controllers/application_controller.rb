class ApplicationController < ActionController::Base
  before_action :init_site

  def init_site
    session[:basket] ||= Array.new
    session[:flag] ||= 'EUR'
    @flag = session[:flag]
    @basket = session[:basket].count
    @buyed = Basket.bought_books(session)
    @buyed_total = Basket.total_bought_books(@buyed)
  end
end
