class BasketController < ApplicationController
  def set_book
    @book = Book.find(params[:id])
    session[:basket] << params[:id]
    redirect_to '/'
  end

  def reset_basket
    session[:basket] = Array.new
    redirect_to '/'
  end

  def delete_basket_book
    @book=Book.find(params[:id])
    # delete first params[:id] in session[:basket]
    session[:basket].delete_at(session[:basket].find_index(@book.id.to_s))
    redirect_to '/'
  end

  def change_flat
    session[:flag] = Basket.change_flag(session)
    redirect_to '/'
  end
end
