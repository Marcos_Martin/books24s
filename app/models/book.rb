class Book < ApplicationRecord
  def with_tax?
    kind_price ? 'with tax':'base'
  end

  def base_price(quantity)
    if kind_price
      vats = YAML.load_file("#{Rails.root}/config/vats.yml")
      vat_book = vats[country]
      price_vat = price - (price * vat_book)
      price_vat * (1-(discount/100)) * quantity
    else
      price * (1-(discount/100)) * quantity
    end
  end

  def tax(flag, base_price)
    vats = YAML.load_file("#{Rails.root}/config/vats.yml")
    vat_book = vats[country]
    if flag == 'EUR'
      if kind_price
        base_price - (base_price / (1 + vat_book))
      else
        base_price * vat_book
      end
    elsif flag == 'WORLD'
      0
    end
  end

end
