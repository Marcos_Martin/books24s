class Basket
  def self.change_flag(session)
    solve = ''
    if session[:flag] == 'EUR'
      solve ='WORLD'
    end
    if session[:flag] == 'WORLD'
      solve ='EUR'
    end
    solve
  end
  def self.bought_books(session)
    # solve (array of arrays):
    # [
    # tuple1-> [id_book1, quantity , base price, tax, price with IVA to pay],
    # tuple2-> [id_book2, cantidad , El precio base de la compra, Los impuestos a pagar por la compra, El precio con IVA a pagar por la compra],
    # ...
    # ]
    solve = Array.new
    session_basket = session[:basket]
    session_flag = session[:flag]
    basket_clean = session_basket.uniq # ids not repeated

    if session_basket.count > 0
      basket_clean.each do |id_book|
        book = Book.find(id_book)

        tuple = Array.new
        tuple << id_book

        quantity = session_basket.count(id_book)
        tuple << quantity

        base_price = book.base_price(quantity)
        tuple << base_price.round(2)

        tax = book.tax(session_flag, base_price)
        tuple << tax.round(2)

        price_with_tax = base_price + tax
        tuple << price_with_tax.round(2)

        solve << tuple
      end
    end
    solve
  end
  def self.total_bought_books(bought_books)
    base_price = 0
    tax = 0
    base_price_with_tax = 0
    solve = Array.new
    bought_books.each do |book|
      base_price += book[2]
      tax += book[3]
      base_price_with_tax += book[4]
    end
    solve << base_price
    solve << tax
    solve << base_price_with_tax
    solve
  end
end