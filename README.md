# PARTE1
**Teoría:** Abajo están los lineamientos y la pregunta. Para enviarnos la respuesta, nos bastaría recibir un diagrama con el resultado de tu análisis, 
puede ser hecho a mano (con una foto nos sirve), o como quieras. 
Necesitamos que sea comprensible para que podamos revisar tu razonamiento. 
Puedes incluir diagramas y textos (breves) con premisas, o lo que consideres pertinente. Por favor envianos la respuesta en inglés ;)

Tenemos users, bookshelves, editions (los libros en sí), chapters, prices y readings (lecturas). 
Users have bookshelves, in the bookshelves you find books, books have chapters and prices, and users read the books.
¿Qué estructura de clases / tablas usuarías para hacer un MVP?


# PARTE 2
**Práctica:** Aquí esperamos  como resultado el código en RubyOnRails, sin excepciones y a poder ser que devuelva los resultados correctos.

Dados un precio, la indicación de si es "precio base" o "con impuestos", el número de lecturas del libro, el país y el porcentaje de descuento. 
Adjunto encontrarás un fichero con la información de los IVAs (VATs) en Europa __(si el país de lectura es fuera de Europa, el IVA se considerará 0)__. 
El fichero adjunto puede tener un formato no válido para Python y es sólo a título informativo para evitar que tengas que buscar los datos en internet, 
además, los valores se incluyen en decimal, es decir, el 21% de IVA para España se refleja como 0.21. 
Teniendo en cuenta esto, como prefieras usar / cargar la información es parte de tu implementación.

Calcula:
1. El precio base de la compra (precio base * (1-descuento) * cantidad)
2. Los impuestos a pagar por la compra
3. El precio con IVA a pagar por la compra

## Instalación
1. Compilar el proyecto con `bundle install`
2. Ejecutar `rake database:reset` y se inicializará la base de datos con datos de prueba.
3. Ejecutar `rails s` y se arrancará el proyecto en `localhost:3000` en modo desarrollo

La solución a la parte 1, está en el directorio PARTE1. La parte 2 es este mismo proyecto.