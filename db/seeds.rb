for i in 1..12
  name = Faker::Book.title
  image = Faker::Avatar.image
  price = rand(0.0..80.0).round(2)
  kind_price = rand(0..1) # 0 = base (false), 1 = with tax (true)
  readed = rand(0.0..80.0)
  country = YAML.load_file("#{Rails.root}/config/vats.yml").map { |a , b| a }.sample
  discount = rand(0.0..100.0).round(2)
  Book.create(name: name, image: image, price: price, kind_price: kind_price,
              readed: readed, country: country, discount: discount)
end