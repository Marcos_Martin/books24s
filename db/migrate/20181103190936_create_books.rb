class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name
      t.string :image
      t.decimal :price
      t.boolean :kind_price # 0 = base (false), 1 = with tax (true)
      t.integer :readed
      t.string :country
      t.decimal :discount

      t.timestamps
    end
  end
end
